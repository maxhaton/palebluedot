# PBD, is a toy compiler backend. 

The main goal is to optimize the framework for ease of use and utilise the 
advanced metaprogramming capabilities of the D language to make declerative 
generation of optimization passes more easy - for example, generating peephole 
optimizations from textual specification and generating the IR instructions from
specifications too + their lowerings.


*Current State: Not even close* 
