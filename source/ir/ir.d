///Contains IR boilerplate and correctness checking.
module ir;
import std.array;
///Basic internal representation node
public interface IntRepNode {
  ///Give textual representation. Purity for obvious reasons.
  pure string toTextual();
  ///Get name of Node, may be null.
  pure const string getName();
  pure const  uint getSize();
}
///Basic IntRep type.
public class IntRepType : IntRepNode {
  private string typename;
  private uint size;
  pure const uint getSize()
  {
    return size;
  }
  pure const string toTextual()
  {
    return typename;
  }
  pure const string getName()
  {
    return typename;
  }
}
///A type tuple, e.g. a constant length structure of types.
public const class IntRepTypeTuple : IntRepType {
  public uint length;
  IntRepType[] types;
  override pure uint getSize()
  {
    uint szCount = 0;
    foreach(i; types)
      szCount += i.getSize();
    return szCount;
  }
  ///Give textual reprentation i.e. <i32, i64, mytype>
  override pure const string toTextual()
  {
    import std.range, std.algorithm;
    string construct = "<";
    string getTextHelper(const(IntRepType) x)
    {
      return x.toTextual();
    }
    construct.puts( types.map!(getTextHelper).joiner(","));
    return construct;
    
  }
}
/*
  A Basic Block is a group of instructions, only containing a single path
  e.g. entrance to branch.
*/
public const class IntRepBasicBlock : IntRepNode {
  IntRepNode[] nodeRange;
  alias nodeRange this;
  pure string toTextual() {return "BasicBlock";};
}

class IntRepModule : IntRepNode {
  string moduleName;
  IntRepBasicBlock constructor;
}
